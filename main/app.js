function distance(first, second){
	
	
	if((first instanceof Array)&&(second instanceof Array)){
		
		if(second===[]&&first===[])
		return 0;

		const setFirst =new Set(first);
		const setSecond=new Set(second);
		const uniqueF=[...setFirst];
		const uniqueS=[...setSecond];
		var distanta=0;

	for(i=0;i<uniqueF.length;i++)
	{
		if(!(uniqueS.includes(uniqueF[i])))
		distanta++;
	}

	for(i=0;i<uniqueS.length;i++)
	{
		if(!(uniqueF.includes(uniqueS[i])))
		distanta++;
	}
	return distanta;
} 
else throw new Error("InvalidType")

}
module.exports.distance = distance